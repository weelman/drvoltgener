<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700,900&amp;subset=cyrillic,cyrillic-ext" rel="stylesheet">
    <title>404 | Доктор Вольт</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <style>
        a:hover {
            opacity: .7;
            transition: 1s;
        }
    </style>
</head>
<body>
    <div style="width: 70vw;
    max-width: 95%;
    margin: auto;
    position: relative;
    display: flex; height: 100vh;">
    <div style="display: flex;
    margin: auto 0;
    width: 100%; font-family: ROBOTO;">
        <div style="width:50%; color: #6a6a6a; text-align:right; font-style: italic; font-weight: bold;"><h1 style="font-size: 260px; margin: 0">404</h1>
        <h3 style="font-size: 2.5em; margin: 0">ТАКОЙ СТРАНИЦЫ НЕТ</h3></div>
        <div style="width:50%;display: flex">
            <div style="margin:auto; padding-left: 5vw;">
                <img style="width:50%;" src="/img/icons/logo.png" alt=""><br>
                <a href="/" style="padding: 1em 2em; background: #0c9b0c; color: #FFF; text-decoration:none; margin: 1em 2px; margin: 19px 9px 0;
    display: inline-block;">ВЕРНУТЬСЯ НА ГЛАВНУЮ</a>
            </div>
        </div>
    </div>
    </div>
</body>
</html>
