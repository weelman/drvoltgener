            //click on a navbar
$('.gogopowerrangers').click(function (event) {
    event.preventDefault();
    var id  = $(this).attr('href'),
        top = $(id).offset().top;
    $('body,html').animate({scrollTop: top}, 1500);
});
$('#ggpr').on('click', function(){
    var top = $('.front-block-fourth').offset().top;
    $('body,html').animate({scrollTop: top}, 1500);
});

            //address desctop
$('#givemeaddress').on('mouseenter', function(){
    if(!$('.inaddress').hasClass("showaddress")) {
        $('.inaddress').addClass('showaddress');
    }
});
$('#givemeaddress').on('mouseleave', function(){
    if($('.alladdress').is(':hover'))
        return false;
    if($('.inaddress').hasClass("showaddress")) {
        $('.inaddress').removeClass('showaddress');
    }
});
$('.inaddress').on('mouseleave', function(){
    if($('#givemeaddress').is(':hover'))
        return false;
    $(this).removeClass('showaddress');
});
$('#menumobile').click(function(){
    $('.mobilemenu').slideToggle();
});

$('#givememobaddress').click(function(){
    var top = $('#map').offset().top;
    $('body,html').animate({scrollTop: top}, 1500);
});



                //confidential

$('.confidential').click(function(){
    $('.politota').fadeIn(500);
});
$(document).mousedown(function (e){
    var div = $(".inpolitota");
    if (!div.is(e.target)
        && div.has(e.target).length === 0) {
        $('.politota').fadeOut(500);
    }
});
$('.close').click(function(){
    $(this).parent().parent().fadeOut(500);
});

                //form

$(document).on('submit', '#form', function(e){
    var data_form = $(this).serialize();
    console.log(data_form);
    e.preventDefault();
    $.ajax({
            type: "POST", //Метод отправки
            url: "/sendmail.php", //путь до php фаила отправителя
            data: data_form,
            success: function() {
//                alert("Заявка успешно отправлена!");
                 $('.gowindow').fadeIn(500);
                 setTimeout(function () {
                    $('.gowindow').fadeOut(500);
                 }, 2000);
                $('#form').trigger('reset');
                return false;
                },
            error: function() {
                $('.gowindow').fadeIn(500);
                 setTimeout(function () {
                    $('.gowindow').fadeOut(500);
                 }, 2000);
                $('#form').trigger('reset');
                return false;
                },
            });
});
