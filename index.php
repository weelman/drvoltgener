<!DOCTYPE html>
<html lang="ru">

<head>
    <meta http-equiv="Cache-control" content="no-cache">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="rights" content="Разработчик Litvinenko Digital">
    <meta name="description" content="Качественный ремонт автоэлектрики в Иркутске. Диагностика, замена, ремонт и техническое обслуживание генераторов и стартеров. Торгово-сервисная сеть Доктор Вольт.">
    <meta name="keywords" content="ремонту стартеров,продаже стартеров,ремонт автоэлектрики,ремонт подвески,качественный ремонт,ремонт дизельных,кузовному ремонту,городе иркутске,нашей комании,доктор вольт,ремонт генераторов иркутск">
    <meta name=«title» content="Ремонт генераторов Иркутск - Доктор Вольт">
    <link rel="canonical" href="http://remont-generatorov-irkutsk.ru/">
    <meta property="og:locale" content="ru_RU">
    <meta property="og:url" content="http://remont-generatorov-irkutsk.ru/">
    <title>Ремонт генераторов Иркутск - Доктор Вольт</title>
    <!--  icon  -->
    <link rel="shortcut icon" href="/favicon.ico">
    <link rel="icon" type="image/png" href="/favicon.ico">
    <link rel="apple-touch-icon" href="/favicon.ico">
    <!--  css   -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/index.css">
    <link rel="stylesheet" href="/css/animate.min.css">
    <script src="/js/wow.min.js">
    </script>
    <script>
        new WOW().init();
    </script>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,600,700,800,900&amp;subset=cyrillic,cyrillic-ext" rel="stylesheet">
    <meta name="yandex-verification" content="35eef6afa031d51f" />
</head>
<body>
    <div class="gowindow" style="display:none">
        <div class="gowindow2">
            <img class="close" src="/img/icons/close-w.png" alt="">
            <div class="gowindow3 text-center">
                <img src="img/icons/okay.jpg" alt="">
                <h3>Спасибо!<br>
                Ваша заявка отправлена.<br>
                Мы свяжемся с вами<br>
                в близжайшее время</h3>
                <img class="windlogo" src="img/icons/logo.png" alt="">
            </div>
        </div>
    </div>
    <div class="politota">
      <div class="inpolitota">
       <img class="close" src="/img/icons/close-w.png" alt="">
        <div class="readme">
            <strong>Политика в отношении обработки персональных данных в ООО «Доктор Вольт»<br></strong>
            <br>
            1. Общие положения<br>
            <br>
            1.1. Политика в отношении обработки персональных данных (далее — Политика)направлена на защиту прав и свобод физических лиц, персональные данные которых обрабатывает ООО «Доктор Вольт» (далее — Оператор). <br>
            1.2. Политика разработана в соответствии с п. 2 ч. 1 ст. 18.1 Федерального закона от 27 июля2006 г. № 152-ФЗ «О персональных данных» (далее — ФЗ «О персональных данных»). <br>
            1.3. Политика содержит сведения, подлежащие раскрытию в соответствии с ч. 1 ст. 14 ФЗ «О персональных данных», и является общедоступным документом. <br>
            <br>
            2. Сведения об операторе<br>
            <br>
            2.1. Оператор ведет свою деятельность по адресу Иркутская область, Иркутский район, с. Хомутово, ул. Кирова, 124 <br>
            2.2. Директор: Ильчук Иван Васильевич (телефон +7 914 926 62 36 назначен ответственным за организацию обработки персональных данных. <br>
            2.3. База данных информации, содержащей персональные данные граждан Российской Федерации, находится по адресу: 966236@mail.ru <br>
            <br>
            3. Сведения об обработке персональных данных<br>
            <br>
            3.1. Оператор обрабатывает персональные данные на законной и справедливой основе для выполнения возложенных законодательством функций, полномочий и обязанностей, осуществления прав и законных интересов Оператора, работников Оператора и третьих лиц. <br>
            3.2. Оператор получает персональные данные непосредственно у субъектов персональных данных. <br>3.3. Оператор обрабатывает персональные данные автоматизированным и неавтоматизированным способами, с использованием средств вычислительной техники и без использования таких средств. <br>3.4. Действия по обработке персональных данных включают сбор, запись, систематизацию, накопление, хранение, уточнение (обновление, изменение), извлечение, использование, передачу (распространение, предоставление, доступ), обезличивание, блокирование, удаление и уничтожение. <br>3.5. Базы данных информации, содержащей персональные данные граждан Российской Федерации, находятся на территории Российской Федерации. <br>
            <br>
            4. Обработка персональных данных клиентов<br><br>
            4.1. Оператор обрабатывает персональные данные клиентов в рамках правоотношений с Оператором, урегулированных частью второй Гражданского Кодекса Российской Федерации от 26 января 1996 г. № 14-ФЗ, (далее — клиентов). <br>4.2. Оператор обрабатывает персональные данные клиентов в целях соблюдения норм законодательства РФ, а также с целью: — заключать и выполнять обязательства по договорам с клиентами; — осуществлять виды деятельности, предусмотренные учредительными документами агентства Литвиненко digital ( Litvinenko digital ) — информировать о новых продуктах, специальных акциях и предложениях; — информировать о новых статьях, видео и мероприятиях; — выявлять потребность в продуктах; — определять уровень удовлетворённости работы.<br>4.3. Оператор обрабатывает персональные данные клиентов с их согласия, предоставляемого на срок действия заключенных с ними договоров. В случаях, предусмотренных ФЗ «О персональных данных», согласие предоставляется в письменном виде. В иных случаях согласие считается полученным при заключении договора или при совершении конклюдентных действий. <br>
            4.4. Оператор обрабатывает персональные данные клиентов в течение сроков действия заключенных с ними договоров. Оператор может обрабатывать персональные данные клиентов после окончания сроков действия заключенных с ними договоров в течение срока, установленного п. 5 ч. 3 ст. 24 части первой НК РФ, ч. 1 ст. 29 ФЗ «О бухгалтерском учёте» и иными нормативными правовыми актами.<br> 4.5. Оператор обрабатывает следующие персональные данные клиентов: — Фамилия, имя, отчество; — Тип, серия и номер документа, удостоверяющего личность; — Дата выдачи документа, удостоверяющего личность, и информация о выдавшем его органе; — Год рождения; — Месяц рождения; — Дата рождения; — Место рождения; — Адрес; — Номер контактного телефона; — Адрес электронной почты; — Идентификационный номер налогоплательщика; — Номер страхового свидетельства государственного пенсионного страхования; — Должность; — Фотография.<br> 4.6. Для достижения целей обработки персональных данных и с согласия клиентов Оператор предоставляет персональные данные или поручает их обработку следующим лицам: — менеджер по продажам — руководитель проекта — менеджер проекта — маркетолог<br>
            <br>
            5. Сведения об обеспечении безопасности персональных данных<br>
            <br>
            5.1. Оператор назначает ответственного за организацию обработки персональных данных для выполнения обязанностей, предусмотренных ФЗ «О персональных данных» и принятыми в соответствии с ним нормативными правовыми актами. <br>5.2. Оператор применяет комплекс правовых, организационных и технических мер по обеспечению безопасности персональных данных для обеспечения конфиденциальности персональных данных и их защиты от неправомерных действий: <br>— обеспечивает неограниченный доступ к Политике, копия которой размещена по адресу нахождения Оператора, а также может быть размещена на сайте Оператора (при его наличии); <br>— во исполнение Политики утверждает и приводит в действие документ «Положение об обработке персональных данных» (далее — Положение) и иные локальные акты; <br>— производит ознакомление работников с положениями законодательства о персональных данных, а также с Политикой и Положением; <br>— осуществляет допуск работников к персональным данным, обрабатываемым в информационной системе Оператора, а также к их материальным носителям только для выполнения трудовых обязанностей; <br>— устанавливает правила доступа к персональным данным, обрабатываемым в информационной системе Оператора, а также обеспечивает регистрацию и учёт всех действий с ними; <br>— производит оценку вреда, который может быть причинен субъектам персональных данных в случае нарушения ФЗ «О персональных данных»; <br>— производит определение угроз безопасности персональных данных при их обработке в информационной системе Оператора; <br>— применяет организационные и технические меры и использует средства защиты информации, необходимые для достижения установленного уровня защищенности персональных данных; <br>— осуществляет обнаружение фактов несанкционированного доступа к персональным данным и принимает меры по реагированию, включая восстановление персональных данных, модифицированных или уничтоженных вследствие несанкционированного доступа к ним; <br>— производит оценку эффективности принимаемых мер по обеспечению безопасности персональных данных до ввода в эксплуатацию информационной системы Оператора; <br>— осуществляет внутренний контроль соответствия обработки персональных данных ФЗ «О персональных данных», принятым в соответствии с ним нормативным правовым актам, требованиям к защите персональных данных, Политике, Положению и иным локальным актам, включающий контроль за принимаемыми мерами по обеспечению безопасности персональных данных и их уровня защищенности при обработке в информационной системе Оператора. <br><br>
            6. Права субъектов персональных данных<br>
            <br>
            6.1. Субъект персональных данных имеет право: <br>— на получение персональных данных, относящихся к данному субъекту, и информации, касающейся их обработки; <br>— на уточнение, блокирование или уничтожение его персональных данных в случае, если они являются неполными, устаревшими, неточными, незаконно полученными или не являются необходимыми для заявленной цели обработки; <br>— на отзыв данного им согласия на обработку персональных данных; <br>— на защиту своих прав и законных интересов, в том числе на возмещение убытков и компенсацию морального вреда в судебном порядке; <br>— на обжалование действий или бездействия Оператора в уполномоченный орган по защите прав субъектов персональных данных или в судебном порядке. <br>6.2. Для реализации своих прав и законных интересов субъекты персональных данных имеют право обратиться к Оператору либо направить запрос лично или с помощью представителя. Запрос должен содержать сведения, указанные в ч. 3 ст. 14 ФЗ «О персональных данных».
            <br>
            <br>
            <i><small>УТВЕРЖДАЮ<br> Генеральный директор<br> Ильчук Иван Васильевич<br><br> 25.01.2018</small></i>
        </div>
      </div>
    </div>
    <nav class="navbar navbar-expand-lg navbar-light">
        <div class="container">
            <div class="row justify-content-around w-100">
                    <div class="col-4 col-md-auto text-center">
                        <img class="logo" src="img/icons/logo.png" alt="Логотип">
                    </div>
                    <div class="col col-md-auto align-self-center text-center">
                        <a href="tel:89842719868" class="number">8 (984) 271-98-68</a><br>
                        <div id="givemeaddress">
                            <img class="address" src="img/icons/icoaddress.png" alt="Иконка адрес"><span class="navaddress">Адреса филиалов</span>
                        </div>
                        <div id="givememobaddress">
                            <img class="address" src="img/icons/icoaddress.png" alt="Иконка адрес"><span class="navaddress">Адреса филиалов</span>
                        </div>
                        <div class="alladdress text-left">
                            <div class="inaddress">
                               <br>
                                <p>Наши адреса в Иркутске:</p>
                                <address class="gogopowerrangers" href="#map">Лызина 13</address>
                                <address class="gogopowerrangers" href="#map">Лермонтова 124а/1</address>
                                <address class="gogopowerrangers" href="#map">Сергеева 3/1</address>
                                <address class="gogopowerrangers" href="#map">Баррикад 26/6</address>
                                <address class="gogopowerrangers" href="#map">Центральная 7б</address>
                                <p>Наш адрес в Шелехово:</p>
                                <address class="gogopowerrangers" href="#map">Култукский тракт, 18</address>
                                <p>Наш адрес в Ангарске:</p>
                                <address class="gogopowerrangers" href="#map">258-й квартал, ст 301</address>
                            </div>
                        </div>
                    </div>
                    <div class="col-4 col-md-auto d-none d-md-block">
                        <div class="row align-self-center navgo">
                            <div class="collapse navbar-collapse" id="navbarNav">
                                <div class="navbar-nav text-center">
                                  <a class="nav-item nav-link">генераторы<span class="sr-only">(current)</span></a>
                                  <a class="nav-item nav-link" href="http://akkumulyatory-irkutsk.ru" target="_blank">аккумуляторы</a>
                                  <a class="nav-item nav-link" href="http://remont-starterov-irkutsk.ru/" target="_blank">стартеры</a>
                                  <a class="nav-item nav-link gogopowerrangers" href="#allaboutus">о компании</a>
                                  <a class="nav-item nav-link gogopowerrangers" href="#map">контакты</a>
                                </div>
                            </div>
                        </div>
                    </div>
                <button class="navbar-toggler align-self-center col-2" id="menumobile" type="button">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>
        </div>
    </nav>
    <div class="mobilemenu">
        <div class="navbar-nav text-center">
          <a class="nav-item nav-link">генераторы<span class="sr-only">(current)</span></a>
          <a class="nav-item nav-link" href="http://akkumulyatory-irkutsk.ru" target="_blank">аккумуляторы</a>
          <a class="nav-item nav-link" href="http://remont-starterov-irkutsk.ru/" target="_blank">стартеры</a>
          <a class="nav-item nav-link gogopowerrangers" href="#allaboutus">о компании</a>
          <a class="nav-item nav-link gogopowerrangers" href="#map">контакты</a>
        </div>
    </div>
    <section class="front-block-first">
        <div class="container">
            <div class="row">
                <div class="col-md-6 text-uppercase">
                    <h1>Ремонт генераторов <br>
                    <big>Иркутск</big></h1>
                    <p class="text-lowercase">
                    <span class="wow fadeInLeft">• комплектующие</span><br>
                    <span class="wow fadeInLeft" data-wow-delay="0.5s">• съем/установка</span><br>
                    <span class="wow fadeInLeft" data-wow-delay="1s">• обслуживание</span><br>
                    <span class="wow fadeInLeft" data-wow-delay="1.5s">• доставка</span><br>
                    <span class="wow fadeInLeft" data-wow-delay="2s">• продажа</span><br>
                    <span class="wow fadeInLeft" data-wow-delay="2.5s">• ремонт</span></p>
                    <div class="col-md-6 p-0 text-center">
                        <button class="wow flash" id="ggpr" data-wow-delay="1s">записаться на сервис</button><br>
                    </div>
                </div>
                <div class="col-md-5 offset-md-1">
                    <img class="w-100" src="img/first/gener.png" alt="">
                </div>
            </div>
        </div>
    </section>
    <section class="front-block-second">
        <div class="container">
            <div class="row">
                <div class="col-md-6 fsb-1">
                    <img id="hummerandkey" src="img/second/H&K.jpg" alt="">
                    <h3 class="wow fadeInDown">
                        Восстановление<br>
                        и замена генераторов
                    </h3>
                    <p class="mb-0 mt-3">Мы производим ремонт генераторов с использованием<br>
                    современного оборудования диагностики и<br>
                    металлообработки.<br>
                    Собственный склад запчастей позволяет нам устранить<br>
                    проблему в короткие сроки и без ожидания.<br>
                    На комплектующие и на свои работы даем гарантию.</p>
                </div>
                <div class="col-md-6 fsb-2">
                    <div class="row h-100 align-items-end text-center">
                        <div class="col-md-4 col-12"><img src="img/second/first.jpg" alt="">
                        <h5>Диагностика генератора</h5>
                        <p>в течение 15 минут<br><br></p></div>
                        <div class="col-md-4 col-12"><img src="img/second/second.jpg" alt="">
                        <h5>Гарантия</h5>
                        <p>на ремонтные работы <br>
                        и комплектующие</p></div>
                        <div class="col-md-4 col-12"><img src="img/second/third.jpg" alt="">
                        <h5>Наличие на складе</h5>
                        <p>собственный склад<br>
                        более 7 тыс. позиций</p></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="front-block-third">
        <div class="container">
            <div class="col-md-11 offset-md-1">
                <h2 class="wow fadeInDown" data-wow-delay="0.3s">Как происходит ремонт генераторов</h2>
            </div>
            <div class="row offset-md-1 ftb-1">
                <div class="col-md-2">
                    <img src="img/third/1.jpg" alt="">
                    <h4>Щетки генератора</h4>
                    <p>При их износе появляется<br>световой сигнал<br>(контрольная лампа).<br>Изношенные щётки<br>подлежат обязательной<br>замене.</p>
                </div>
                <div class="col-md-2">
                    <img src="img/third/2.jpg" alt="">
                    <h4>Обмотка статора</h4>
                    <p>При выгорании или<br>замыкании витков<br>обмотки появляется<br>шум и запах гари. Такая<br>обмотка меняется<br>на новую.</p>
                </div>
                <div class="col-md-2">
                    <img src="img/third/3.jpg" alt="">
                    <h4>Контактные кольца</h4>
                    <p>Замена требуется в<br>случае их прогорания<br>или износа.</p>
                </div>
                <div class="col-md-2">
                    <img src="img/third/4.jpg" alt="">
                    <h4>Диодный мост</h4>
                    <p>Диоды в выпрямительном<br>блоке прозваниваются,<br>пробитые меняем на<br>новые.</p>
                </div>
            </div>
            <div class="row offset-md-1 ftb-1">
                <div class="col-md-2">
                    <img src="img/third/5.jpg" alt="">
                    <h4>Обгонный шкив<br>(муфта)</h4>
                    <p>Изношенный или<br>поврежденный шкив<br>мы заменяем на<br>аналогичный новый.</p>
                </div>
                <div class="col-md-2">
                    <img src="img/third/6.jpg" alt="">
                    <h4>Подшипник</h4>
                    <p>Появление постороннего<br>шума при работе<br>генератора связано<br>с износом или<br>повреждением<br>подшипников.<br>Изношенные подшипники<br>меняют.</p>
                </div>
                <div class="col-md-2">
                    <img src="img/third/7.jpg" alt="">
                    <h4>Реле-регулятор<br>напряжения</h4>
                    <p>Неисправный регулятор<br>подлежит замене, так<br>как зачастую  является<br>неразборным, а,<br>следовательно, и<br>неремонтопригодным.</p>
                </div>
                <div class="col-md-3">
                    <img class="mt-3" src="img/icons/logo.png" alt="">
                 </div>
            </div>
            <div class="row scrolltext offset-md-1 col-md-9 col-12 mt-4">
                <p style="font-size:.9em; margin-top:.5em;">Во первых, мы определим неисправности и рассчитаем стоимость ремонта, предварительно проверив параметры работы и целостность ключевых узлов вашего агрегата. <br>
                    <br>
                Во время диагностики измерим сопротивление обмоток возбуждения и диодов в выпрямительном блоке. Исправность реле-регулятора проверяем на спец. стенде под нагрузкой. Определяем уровень изношенности щёток, наглядно оцениваем общее состояние конструктивных деталей, в том числе подшипников и шкива.<br>
                <br>
                По результатам диагностики осуществляем необходимый вид ремонта генераторов автомобиля.<br>А также выполняем следующие работы:
                <br>
                затягиваем гайку шкива;<br>
                зачищаем от коррозии клеммы;<br>
                проверяем изоляцию проводов;<br>
                укладываем провода на расстоянии не менее 30 миллиметров от регулятора напряжения, чтобы при нагреве не расплавился изоляционный слой;<br>
                восстанавливаем изоляцию обмоток.</p>
            </div>
        </div>
    </section>

    <section class="front-block-fourth">
        <div class="container">
            <div class="row">
                <div class="col-3 mob-hid text-right">
                    <img class="w-75" src="img/fourth/first.jpg" alt="">
                </div>
                <div class="col-md-7 col-12 text-center">
                    <h3 class="wow fadeInDown">Запишитесь на ремонт<span class="mob-hid">, замену или<br>
                        получите бесплатную консультацию</span></h3>
                    <div class="insection1">
                        <div class="row m-0">
                            <div class="col-md-4 col-12">
                                <img src="img/icons/logo.png" alt="">
                                <h5>Заполните форму</h5>
                                <p>мы перезвоним и ответим<br>
                                на все вопросы</p>
                            </div>
                            <div class="col-md-8 col-12">
                                <div class="row justify-content-center">
                                    <form method="POST" id="form">
                                        <div class="row m-0">
                                            <div class="col-md-6 col-12">
                                                <input type="text" placeholder="Введите имя*" name="name" required>
                                                <input type="text" placeholder="Введите телефон*" name="phone" required>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <textarea name="comment" placeholder="Комментарии к звонку"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-auto">
                                            <button class="wow flash" data-wow-delay="1.5s">ПОЛУЧИТЬ КОНСУЛЬТАЦИЮ</button>
                                            <a class="confidential">Я даю свое согласие на обработку<br> персональных данных</a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="front-block-fifth">
        <div class="container">
            <div class="row justify-content-center">
                <h2 class="text-center mb-4">Продажа, диагностика, ремонт стартеров и аккумуляторов<br>для легковых и грузовых автомобилей</h2>
                <div class="col-md-5 col-12 text-center">
                    <img class="w-75" src="img/fifth/1.png" alt="">
                    <p>Стартеры</p>
                    <a href="http://remont-starterov-irkutsk.ru/" target="_blank"><button class="wow flash">подробнее</button></a>
                </div>
                <div class="col-md-5 col-12 text-center">
                    <img class="w-75" src="img/fifth/2.png" alt="">
                    <p>Аккумуляторы</p>
                    <a href="http://akkumulyatory-irkutsk.ru" target="_blank"><button class="wow flash">подробнее</button></a>
                </div>
            </div>
        </div>
    </section>

    <section id="map">
        <h3>Наши контакты</h3>
        <div class="container">
            <div class="row align-items-center">
                <div class="col-sm-auto">
                    <img src="img/icons/logomap.png" alt="">
                    <p>Наш телефон:</p>
                    <div class="wow shake" data-wow-delay="1s">
                        <a href="tel:89842719868">8 (984) 271-98-68</a>
                    </div>
                    <p>Наши адреса в Иркутске:</p>
                    <address>Лызина 13</address>
                    <address>Лермонтова 124а/1</address>
                    <address>Сергеева 3/1</address>
                    <address>Баррикад 26/6</address>
                    <address>Центральная 7б</address>
                    <p>Наш адрес в Шелехово:</p>
                    <address>Култукский тракт, 18</address>
                    <p>Наш адрес в Ангарске:</p>
                    <address>258-й квартал, ст 301</address>
                </div>
                <div class="col">
                     <div class="mapborder">
                         <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A921bd21b4393f06fb79aac5f7019195467ef191275affb0300ca5b4095ca84f2&amp;width=100%&amp;height=548&amp;lang=ru_RU&amp;scroll=true"></script>
                     </div>
                </div>
            </div>
            <div class="row mobile-sn">
                <div class="col"><a href="https://www.instagram.com/doctorvolt138/"><img src="img/icons/insta-black.svg" alt=""></a></div>
                <div class="col"><a href="https://vk.com/akkumulyatory_irkutsk"><img src="img/icons/vk-black.svg" alt=""></a></div>
                <div class="col"><a href="https://www.facebook.com/Доктор-Вольт-357479184719329/"><img src="img/icons/fb-black.svg" alt=""></a></div>
            </div>
        </div>
    </section>
    <div id="allaboutus">
        <h3 class="text-center"><strong>О компании Дoктор Вольт</strong></h3>
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-5">
                    <img src="img/sixth/1.jpg" alt="">
                </div>
                <div class="col">
                    <p>
                        Ремонт автоэлектрики - это наше призвание! <br>
                        Без правильной работы генератора Ваш аккумулятор не будет заряжаться должным образом, а при плохой работе стартера автомобиль и вовсе не заведется. Если ваш аккумулятор плохого качества или уже прошел срок его службы, он не позволит автомобилю запуситить двигатель, поэтому стоит уделить особое внимание к электрике вашего автомобиля.<br>
                        <br>
                        Мы производим качественный ремонт агрегатов не только в городе Иркутске. Наши специалисты сами снимут агрегат, отремонтируют и установят обратно в автомобиль за считанные часы.
                    </p>
                </div>
            </div>
            <div class="row align-items-center">
                <div class="col-md-5">
                    <img src="img/sixth/2.jpg" alt="">
                </div>
                <div class="col">
                <p>Все наши механики имеют большой опыт и проходят обучения по ремонту стартеров, генераторов и аккумуляторов. В продаже стартеров и генераторов имеются зарубежные бренды и все в наличии с нашего склада, что значительно сокращает время замены.<br><br>Наше дигностическое оборудование проверено годами и тысячами часов работы, поэтому мы уверены в точности расчетов.<br><br>Выявить проблему деталей и устранить их вовремя, может значительно сэкономить ваш бюджет в дальнейшем эксплуатировании автомобиля.<br>При неисправности рекомендуем обратиться в один из наших торгово-сервисных центров незамедлительно.
                </p>
                </div>
            </div>
            <div class="row align-items-center">
                <div class="col-md-5">
                    <img src="img/sixth/3.jpg" alt="">
                </div>
                <div class="col">
                    <p>Мы не выполняем ремонт подвески и не оказываем услуги по кузовному ремонту, а также ходовой части. Компания Доктор Вольт зарекомендовала себя как ответственный и надежный партнер автоэлектрики грузовых, легковых автомобилей и спецтехники. <br><br>Все вопросы по техническому обслуживанию вы можете задать нашему менеджеру или оставить заявку на нашем сайте, мы вам перезвоним. <br>
                    Приезжайте на диагностику в один из наших сервисно-торговых пунктов.<br>
                    Вы можете получить скидку от объема работ.</p>
                </div>
            </div>
        </div>
    </div>
    <div id="socnetwork">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-sm-auto">
                    <a class="instagg" href="https://www.instagram.com/doctorvolt138/" target="_blank"><img src="img/icons/instagram.png" alt="instagram"></a>
                </div>
                <div class="col">
                    <h4 class="wow fadeInUp" data-wow-delay="1s">мы в социальных сетях<br>
                    НОВОСТИ, АКЦИИ, СКИДКИ. ПРИСОЕДИНЯЙТЕСЬ</h4>
                </div>
            </div>
            <div class="row justify-content-xl-left" style="margin-top:2em;">
                <div class="col-lg-6">
                        <script type="text/javascript" src="//vk.com/js/api/openapi.js?151"></script>
                                    <!-- VK Widget -->
                        <div id="vk_groups"></div>
                        <script type="text/javascript">
                        VK.Widgets.Group("vk_groups", {mode: 3, width: 'auto'}, 158058993);
                        </script>
                </div>
                <div class="col-lg-6 text-center">
                    <div class="fb-page" data-href="https://www.facebook.com/Доктор-Вольт-357479184719329/" data-tabs="timeline" data-width="450" data-height="182" data-small-header="true" data-adapt-container-width="false" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/Доктор-Вольт-357479184719329/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/Доктор-Вольт-357479184719329/">Доктор вольт</a></blockquote></div>
                </div>
            </div>
        </div>
    </div>
    <div class="developer text-center">
        <a href="https://litvinenko.digital/" target="_blank">разработка сайта - LITVINENKO</a>
    </div>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="js/index.js"></script>
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = 'https://connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.11';
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
</body>
</html>
